# Phaser TS Webpack

This repository provides a project boilerplate to create your own game with Phaser, Typescript and Webpack without some much configuration.

## Installation

Clone this repository:

```bash
git clone git@gitlab.com:ChechuMP/phaser-ts-webpack.git
```

Install dependencies:

```bash
cd phaser-ts-webpack
npm install
```

Run it:

```bash
npm start
```

## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Roadmap
[CHANGELOG](https://gitlab.com/ChechuMP/phaser-ts-webpack/-/blob/main/CHANGELOG.md)

## License
[MIT](https://choosealicense.com/licenses/mit/)
