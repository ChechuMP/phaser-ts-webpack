# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [develop]
- Testing.
- Webpack configuration depending on environment (development, staging, production...).

## [0.1.0] - 2021-12-11
### Added
- Typescript.
- Basic code format and linting.
- Webpack configuration and plugins.

[develop]: https://gitlab.com/ChechuMP/phaser-ts-webpack/compare/v0.1.0...develop
[0.1.0]: https://gitlab.com/ChechuMP/phaser-ts-webpack/tag/v0.1.0
