/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: './src/index.ts',
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [{
      test: /\.(ts)$/,
      exclude: /node_modules/,
      use: {
        loader: 'ts-loader'
      }
    }]
  },
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      '@root': path.resolve(__dirname, 'src'),
      '@config': path.resolve(__dirname, 'src', 'config'),
      '@scenes': path.resolve(__dirname, 'src', 'scenes')
    }
  },
  devServer: {
    open: true,
    watchFiles: ['public/**/*']
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'public/index.html'
    })
  ],
}
