import launch from '@root/launch'
import gameConfig from '@config/game'

import Main from '@scenes/main'

window.onload = () => {
  const canvas = document.getElementById('game')
  if (canvas === null || !(canvas instanceof HTMLCanvasElement)) {
    throw new Error('Game can be launched!')
  }

  launch(Main, gameConfig, canvas)
}
