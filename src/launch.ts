import Phaser from 'phaser'
import Game = Phaser.Game
import Scene = Phaser.Scene
import GameConfig = Phaser.Types.Core.GameConfig

export default function launch(scene: typeof Scene, config: GameConfig, canvas: HTMLCanvasElement): void {
  config.scene = scene
  config.canvas = canvas
  new Game(config)
}
