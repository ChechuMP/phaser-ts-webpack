import Phaser from 'phaser'
import GameConfig = Phaser.Types.Core.GameConfig

const gameConfig: GameConfig = {
  type: Phaser.CANVAS,
  width: 800,
  height: 600
}

export default gameConfig
